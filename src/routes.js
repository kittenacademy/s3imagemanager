const express = require("express");
const app = express.Router();
const config = require("config");

app.get("/", function (req, res) {
	res.render("index", {
		static_path: "static",
		siteName: config.get("SiteName"),
		user: req.user
	});
});

app.get('/profile',
  require('connect-ensure-login').ensureLoggedIn(),
  function(req, res){
    res.render('profile', { user: req.user });
  });

app.use("/api", require("./api/routes"));
module.exports = app;