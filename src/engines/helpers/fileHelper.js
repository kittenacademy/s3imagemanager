const path = require("path"),
	fs = require("fs"),
	sizeOf = require("image-size"),
	Path = require("path"),
	Mime = require("mime"),
	gifyParse = require("gify-parse");

module.exports.GetImageData = (uploadPath, callback) => {
	const pathobj = Path.parse(uploadPath);
	const filename = pathobj.base;
	const type = Mime.lookup(pathobj.ext);
	const fileSize = fs.statSync(uploadPath).size;
	const uploader = "UNKNOWN";
	let buffer = fs.readFileSync(uploadPath);
	let imageData = {
		type: type,
		filename: filename,
		size: fileSize,
		uploader: uploader,
		uploaded: new Date()
	};
	if (type === "image/gif") {
		const gifInfo = gifyParse.getInfo(buffer);
		if (gifInfo.valid && gifInfo.animated) {
			imageData.height = gifInfo.height;
			imageData.width = gifInfo.width;
			imageData.duration = gifInfo.duration;
			callback(imageData);
		} else {
			callback(null);
		}
	} else {
		const dimensions = sizeOf(uploadPath);
		imageData.height = dimensions.height;
		imageData.width = dimensions.width;
		imageData.duration = 0;
		callback(imageData);
	}
}


const ALPHABET = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

const UNIQUEID_LENGTH = 8;

module.exports.GenerateUniqueID = () => {
  var rtn = '';
  for (var i = 0; i < UNIQUEID_LENGTH; i++) {
    rtn += ALPHABET.charAt(Math.floor(Math.random() * ALPHABET.length));
  }
  return rtn;
}

module.exports.GetFileNameFromUploadedFile = (filename) => {
	var retval = this.RemoveFileExtensionFromFilename(filename);
	retval = retval.replace(/ /g,"-");
	retval = retval+ '-' + this.GenerateUniqueID()
	return  retval;
}

module.exports.RemoveFileExtensionFromFilename = (filename) => {
	return filename.replace(/\.[^/.]+$/, "")
}