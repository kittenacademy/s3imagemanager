//TODO send record to Dyanamodb

const path = require("path"),
	fs = require("fs"),
	sizeOf = require("image-size"),
	s3 = require("../s3"),
	Path = require("path"),
	Mime = require("mime"),
	gifyParse = require("gify-parse"),
	Debug = require("../debug"),
	fileHelper = require("./helpers/fileHelper");

module.exports.upload = (fileUpload, userId, callback) => {// The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file 
	const filename = fileHelper.GetFileNameFromUploadedFile(fileUpload.name);

	const appDir = path.dirname(require.main.filename);
	// Use the mv() method to place the file somewhere on your server 
	const uploadPath = path.join(appDir, "temp", filename);
	Debug.log("uploading", filename);
	Debug.time("downloadfile");
	fileUpload.mv(uploadPath, function (err) {
		Debug.timeEnd("downloadfile");
		if (err) {
			callback({success: false, data: err});
			return;
		}
		Debug.time("uploadfile");
		s3.UploadFromPath(uploadPath, function(uploadSuccess){
			Debug.timeEnd("uploadfile");
			if (!uploadSuccess.success) {
				callback({success: false, data: uploadSuccess.data});
				return;
			}
			Debug.time("imageData");
			fileHelper.GetImageData(uploadPath, function(imageData){
				Debug.timeEnd("imageData");
				callback({success: true, data: {s3: uploadSuccess, fs: imageData}});
				fs.unlinkSync(uploadPath);
				return;
			});
		});
	});
}
