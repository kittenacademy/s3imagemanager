const express = require("express");
const app = express.Router();

app.get("/helloworld", require("./helloworld"));

const fileUpload = require("express-fileupload");

app.use(fileUpload());
app.post("/upload", require("./upload"));
app.use("/auth", require("./auth"));
app.get('/testprofile',
  require('connect-ensure-login').ensureLoggedIn(),
  function(req, res){
    res.render('profile', { user: req.user });
  });

module.exports = app;