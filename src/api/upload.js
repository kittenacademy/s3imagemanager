const fileEngine = require("../engines/fileEngine");;

module.exports = (req, res) => {
	if (!req.user) {
		res.status(401).send("Not logged in");
		return;
	}
	if (!req.files || !req.files.fileUpload) {
		res.send("No files were uploaded.");
		return;
	}
	fileEngine.upload(req.files.fileUpload, req.user.id, (result) => {
		if (!result.success) {
			console.log(result);
			res.status(500).send(result.data);
			return;
		} else {
			res.status(200).send(result.data);
		}
	});
};