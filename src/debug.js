const Enabled = require("config").get("Debug"),
	util = require("util");
const _Reset = "\x1b[0m";
const _Error = "\x1b[31m";
const _Warn = "\x1b[33m";
function GetTime() {
	return new Date().toISOString().
		replace(/T/, " ").
		replace(/\..+/, "");
}
function parseargs(data, ...args) {
	let argstring = "";
	for (let i = 0; i < args.length; i++) {
		argstring += util.format(args[i]) + " ";
	}
	return GetTime() + ": " + data + " " + argstring;
}
/*eslint-disable  no-console */
exports.timeEnd = (key) => {
	if (!Enabled) return;
	console.timeEnd(key);
};
exports.time = (key) => {
	if (!Enabled) return;
	console.time(key);
};
exports.log = (data, ...args) => {
	if (!Enabled) return;
	console.log(" L: ", parseargs(data, ...args)); // eslint-disable-line no-use-before-define
};
exports.warn = (data, ...args) => {
	if (!Enabled) return;
	console.log(_Warn, "W:", _Reset, parseargs(data, ...args));
};
exports.error = (data, ...args) => {
	if (!Enabled) return;
	console.log(_Error, "E:", _Reset, parseargs(data, ...args));
};
/*eslint-enable no-console */

// Reset = "\x1b[0m"
// Bright = "\x1b[1m"
// Dim = "\x1b[2m"
// Underscore = "\x1b[4m"
// Blink = "\x1b[5m"
// Reverse = "\x1b[7m"
// Hidden = "\x1b[8m"

// FgBlack = "\x1b[30m"
// FgRed = "\x1b[31m"
// FgGreen = "\x1b[32m"
// FgYellow = "\x1b[33m"
// FgBlue = "\x1b[34m"
// FgMagenta = "\x1b[35m"
// FgCyan = "\x1b[36m"
// FgWhite = "\x1b[37m"

// BgBlack = "\x1b[40m"
// BgRed = "\x1b[41m"
// BgGreen = "\x1b[42m"
// BgYellow = "\x1b[43m"
// BgBlue = "\x1b[44m"
// BgMagenta = "\x1b[45m"
// BgCyan = "\x1b[46m"
// BgWhite = "\x1b[47m"