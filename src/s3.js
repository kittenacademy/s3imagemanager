const AWS = require("aws-sdk"),
	config = require("config"),
	fs = require("fs"),
	s3 = new AWS.S3({
		apiVersion: "2006-03-01",
		credentials: new AWS.Credentials(
			config.get("AWS.AWS_ACCESS_KEY_ID"),
			config.get("AWS.AWS_SECRET_ACCESS_KEY")
		)
	}),
	s3bucket = config.get("AWS.S3.bucketName"),
	Path = require("path"),
	Mime = require("mime");

function CheckBucketIsFound(buckets) {
	return buckets.Name === s3bucket;
}

exports.CheckCredentials = () => {
	s3.listBuckets(function (err, data) {
		if (err) throw (`Can't authenticate with AWS ${err}`);
		const bucket = data.Buckets.find(CheckBucketIsFound);
		if (!bucket || bucket.length == 0) {
			throw (`Can't find S3 bucket named ${s3bucket}`);
		}
	});
};

exports.UploadFromPath = (path, callback) => {
	const pathobj = Path.parse(path);
	const filename = pathobj.base;
	const mimeType = Mime.lookup(pathobj.ext);
	fs.readFile(path, function (err, data) {
		if (err) throw err; // Something went wrong!
		const params = { 
			Bucket: s3bucket, 
			Key: filename, 
			Body: data, 
			ContentType: mimeType, 
			StorageClass: "REDUCED_REDUNDANCY", 
			CacheControl: "max-age=31536000" 
		};
		s3.upload(params, function (err, data) {
			if (err) {
				callback({success: false, data: err});
			} else {
				callback({success: true, data: data});
			}
		});
	});
};