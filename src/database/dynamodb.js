const AWS = require("aws-sdk"),
	config = require("config"),
	dynamodb = new AWS.DynamoDB({
		region: "us-west-2",
		// apiVersion: "2006-03-01",
		credentials: new AWS.Credentials(
			config.get("AWS.AWS_ACCESS_KEY_ID"),
			config.get("AWS.AWS_SECRET_ACCESS_KEY")
		)
	}),
	dyanomodbTable = config.get("AWS.dyanamodb.tableName");

AWS.config.update({
	region: "us-west-2",
	credentials: new AWS.Credentials(
		config.get("AWS.AWS_ACCESS_KEY_ID"),
		config.get("AWS.AWS_SECRET_ACCESS_KEY")
	)
});

var docClient = new AWS.DynamoDB.DocumentClient()

function CheckTableIsFound(tables) {
	return tables === dyanomodbTable;
}
exports.CheckCredentials = () => {
	dynamodb.listTables(function (err, data) {
		if (err) throw (`Can't authenticate with AWS ${err}`);
		const bucket = data.TableNames.find(CheckTableIsFound);
		if (!bucket || bucket.length == 0) {
			CreateTable(dyanomodbTable);
		}
	});
};

exports.AddRecord = (settings, callback) => {
	var params = {
		TableName: dyanomodbTable + "_" + settings.table,
		Item: settings.item
	};

	docClient.put(params, function (err, data) {
		if (err) {
			callback({ success: false, data: err });
			console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
		} else {
			callback({ success: true, data: data });
			console.log("Added item:", JSON.stringify(data, null, 2));
		}
	});
}

exports.DoesRecordExist = (settings, callback) => {
	const fieldName = settings.field;
	const valueToFind = settings.id;
	// var params = {
	// 	TableName: dyanomodbTable + "_" + settings.table,
	// 	Key: {
	// 		fieldName: valueToFind
	// 	}
	// };
	var params = {
		TableName: dyanomodbTable + "_" + settings.table,
		Key: {}
	};
	params.Key[settings.field] = valueToFind;
	docClient.get(params, function (err, data) {
		if (err) {
			if (err.code == "ResourceNotFoundException") {
				CreateTable(params.TableName, (result) => {
					callback(false);
					return;
				});
			}
			callback(false);
		} else {
			callback(true);
		}
	});
};

function CreateTable(tablename, callback) {
	var params = {
		TableName: tablename,
		KeySchema: [
			{ AttributeName: "id", KeyType: "HASH" }
		],
		AttributeDefinitions: [
			{ AttributeName: "id", AttributeType: "S" }
		],
		ProvisionedThroughput: {
			ReadCapacityUnits: 1,
			WriteCapacityUnits: 1
		}
	};
	dynamodb.createTable(params, function (err, data) {
		if (err) {
			console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
			if (callback) {
				callback({ success: false, data: err });
			}
		} else {
			console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
			if (callback) {
				callback({ success: true, data: data });
			}
		}
	});
}
