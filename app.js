const SourcePath = "./src/";
const express = require("express"),
	Debug = require(SourcePath + "debug"),
	passport = require('passport'),
	Strategy = require('passport-google-oauth2').Strategy,
	config = require("config"),
	bodyParser = require("body-parser");

passport.use(new Strategy({
    clientID: config.get("Google.client_id"),
    clientSecret: config.get("Google.client_secret"),
    callbackURL: 'http://localhost:3000/api/auth/login/google/return'
  },
  function(accessToken, refreshToken, profile, cb) {
    return cb(null, profile);
  }));

passport.serializeUser(function(user, cb) {
  cb(null, user);
});

passport.deserializeUser(function(obj, cb) {
  cb(null, obj);
});

const app = express();
app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: "150mb" }));
app.use("/", require(SourcePath + "routes"));
require(SourcePath + "server");

app.set("view engine", "ejs");
app.set("views", __dirname + "/views");

const port = process.env.PORT || 3000;

app.listen(port, function () {
	Debug.log("Server running at http://127.0.0.1:", port, "/");
});